/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {AuthNav} from './src/components/auth/Welcome';
import {ApolloProvider} from '@apollo/client';
import {apolloClient} from './src/service/GraphQLClient';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
//import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {PortfolioNav} from './src/components/portfolio/Portfolio';
import {MarketTopTopNav} from './src/components/market/MainPage';
import {OrderScreen} from './src/components/order/Order';
//import {SafeAreaProvider} from 'react-native-safe-area-context';
import {RecoilRoot} from 'recoil';

const Stack = createStackNavigator();

export default function App() {
  StatusBar.setHidden(true);
  return (
    <RecoilRoot>
      <ApolloProvider client={apolloClient}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName={'AuthNav'} headerMode="none">
            <Stack.Screen
              name="AuthNav"
              component={AuthNav}
              options={{gestureEnabled: false}}
            />
            <Stack.Screen
              name="MainTabNav"
              component={MainTabNav}
              options={{gestureEnabled: false}}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </ApolloProvider>
    </RecoilRoot>
  );
}

const MainTab = createMaterialBottomTabNavigator();

const activeTabColor = 'blue';
const inactiveTabColor = 'rgb(111,111,111)';
const backgroundColor = 'rgb(222,222,222)';
const MainTabNav = () => {
  return (
    <MainTab.Navigator
      initialRouteName="PortfolioScreenNav"
      activeColor="blue"
      barStyle={{backgroundColor}}>
      <MainTab.Screen
        name="PortfolioScreenNav"
        component={PortfolioNav}
        options={{
          tabBarLabel: 'Portfolio',
          tabBarIcon: ({focused}) => (
            <FontAwesome
              name="money"
              color={focused ? activeTabColor : inactiveTabColor}
              size={22}
            />
          ),
        }}
      />
      <MainTab.Screen
        name="MarketScreenNav"
        component={MarketTopTopNav}
        options={{
          tabBarLabel: 'Market',
          tabBarIcon: ({focused}) => (
            <MaterialCommunityIcons
              name="chart-areaspline"
              //name="analytics-sharp"  //  FontAwesome name="line-chart",
              color={focused ? activeTabColor : inactiveTabColor}
              size={22}
            />
          ),
        }}
      />
      <MainTab.Screen
        name="OrderScreenNav"
        component={OrderScreen}
        options={{
          tabBarLabel: 'Order',
          tabBarIcon: ({focused}) => (
            <MaterialCommunityIcons
              name="note-text-outline"
              //name="analytics-sharp"  //  FontAwesome name="line-chart",
              color={focused ? activeTabColor : inactiveTabColor}
              size={22}
            />
          ),
        }}
      />
    </MainTab.Navigator>
  );
};
