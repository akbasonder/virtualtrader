/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
//import { SafeAreaView } from 'react-native-safe-area-context';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
//import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';
import {Logger} from '../../utils/Logger';
import {ForgotPasswordScreen, LoginScreen} from './Login';
import {retrieveToken} from '../../service/GraphQLClient';
import {ActivityIndicator, Text, Colors} from 'react-native-paper';
import {SignupScreen, userIdState} from './Signup';
import {useSetRecoilState} from 'recoil';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {USER_ID_KEY} from '../../config/constants';

const AuthStack = createStackNavigator();
const SCREEN_WIDTH = Dimensions.get('window').width;
const logger = new Logger('Welcome');

const FirstScreen = () => {
  const navigation = useNavigation();
  const setUserIdState = useSetRecoilState(userIdState);
  useEffect(() => {
    async function init() {
      retrieveToken();
      await FontAwesome.loadFont();
      //await FontAwesome5.loadFont();
      await MaterialIcons.loadFont();
      await MaterialCommunityIcons.loadFont();
      await MaterialCommunityIcons.loadFont();
      await Ionicons.loadFont();
      logger.info('Fonts Loaded');
      //await AsyncStorage.removeItem(USER_ID_KEY);
      const userIdString = await AsyncStorage.getItem(USER_ID_KEY);
      logger.log('userIdString is', userIdString);
      if (userIdString) {
        setUserIdState(parseInt(userIdString));
        setTimeout(() => navigation.navigate('MainTabNav'), 1000);
      } else {
        navigation.navigate('WelcomeNav');
      }
    }
    init();
  }, [navigation, setUserIdState]);
  return (
    <SafeAreaView
      style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" animating={true} color={'blue'} />
    </SafeAreaView>
  );
};
//<SafeAreaView style={{flex: 1, justifyContent: 'space-around', alignItems: 'center'}}>
const WelcomeScreen = () => {
  const navigation = useNavigation();
  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
      }}>
      <View style={{alignItems: 'center', marginTop: 50}}>
        <Text style={{fontSize: 20, color: 'black', fontWeight: 'bold'}}>
          Welcome to Virtual Trader
        </Text>
      </View>

      <View style={{marginBottom: 20}}>
        <TouchableOpacity
          style={[styles.longTouchable, {backgroundColor: Colors.blue500}]}
          onPress={() => {
            navigation.navigate('SignupNav');
          }}>
          <View style={styles.longTouchableView}>
            <Text style={styles.longTouchableText}>Sign Up</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.longTouchable,
            {
              backgroundColor: Colors.grey600,
              marginTop: 20,
            },
          ]}
          onPress={() => {
            navigation.navigate('LoginNav');
          }}>
          <View style={styles.longTouchableView}>
            <Text style={[styles.longTouchableText, {color: 'white'}]}>
              Login
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export const AuthNav = () => {
  return (
    <AuthStack.Navigator initialRouteName="FirstNav">
      <AuthStack.Screen
        name="FirstNav"
        component={FirstScreen}
        options={{headerShown: false}}
      />
      <AuthStack.Screen
        name="WelcomeNav"
        component={WelcomeScreen}
        options={{headerShown: false}}
      />
      <AuthStack.Screen
        name="SignupNav"
        component={SignupScreen}
        options={{headerBackTitle: 'Back', headerTitle: ''}}
      />
      <AuthStack.Screen
        name="LoginNav"
        component={LoginScreen}
        options={{headerBackTitle: 'Back', headerTitle: ''}}
      />
      <AuthStack.Screen
        name="ForgotPasswordNav"
        component={ForgotPasswordScreen}
        options={{headerBackTitle: 'Back', headerTitle: ''}}
      />
    </AuthStack.Navigator>
  );
};

const styles = StyleSheet.create({
  waidAppExplanation: {
    fontSize: 32,
    fontWeight: 'bold',
    color: 'purple',
    padding: 10,
  },
  longTouchable: {
    flexDirection: 'row',
    width: SCREEN_WIDTH - 50,
    height: 50,
    alignItems: 'center',
    borderRadius: 25,
  },
  longTouchableIcon: {
    marginLeft: 30,
  },
  longTouchableView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  longTouchableText: {
    marginLeft: -25,
    fontSize: 20,
    color: 'white',
  },
});
