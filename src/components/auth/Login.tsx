/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation} from '@react-navigation/native';
import {TextInput, Text, Colors, HelperText} from 'react-native-paper';
import md5 from 'md5';
import {gql} from '@apollo/client';
import {apolloClient, authClient} from '../../service/GraphQLClient';
import {TOKEN_KEY} from '../../config/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const logger = new Logger('Login');

const GET_USER_BY_EMAIL = gql`
  query user($email: String!) {
    user(where: {email: $email}) {
      id
      username
      password
    }
  }
`;

const FORGOT_PASSWORD = gql`
  mutation forgotPassword($tokenKey: String!, $email: String!) {
    forgotPassword(tokenKey: $tokenKey, email: $email)
  }
`;

export const LoginScreen = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailMsg, setEmailMsg] = useState('');
  const [passwordMsg, setPasswordMsg] = useState('');
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.loginSreen}>
      <View>
        <View
          style={{height: 50, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 24}}> Welcome back!</Text>
        </View>
        <Text style={{fontWeight: '600', fontSize: 12, color: Colors.grey800}}>
          ACCOUNT INFORMATION
        </Text>
        <View style={{marginTop: 10, height: 50}}>
          <TextInput
            autoCapitalize={'none'}
            autoCorrect={false}
            label={'Email'}
            value={email}
            style={{height: 50}}
            onChangeText={(text) => {
              logger.log('email2 is', text);
              setEmail(text);
              setEmailMsg('');
            }}
          />
          <HelperText
            style={{marginTop: -3}}
            type="error"
            visible={emailMsg !== ''}>
            {emailMsg}
          </HelperText>
        </View>

        <View style={{marginTop: 20, height: 50}}>
          <TextInput
            style={{height: 50}}
            autoCapitalize={'none'}
            autoCorrect={false}
            label={'Password'}
            textContentType={'password'}
            secureTextEntry={true}
            value={password}
            onChangeText={(text) => {
              logger.log('password is', text);
              setPassword(text);
              setPasswordMsg('');
            }}
          />
          <HelperText
            style={{marginTop: -3}}
            type="error"
            visible={passwordMsg !== ''}>
            {passwordMsg}
          </HelperText>
        </View>

        <TouchableOpacity
          style={[
            styles.longTouchable,
            {
              backgroundColor: Colors.blue500,
              marginTop: 20,
            },
          ]}
          onPress={() => {
            logger.log('md5 result', md5(password));
            let hasError = false;
            if (email.indexOf('@') <= 0) {
              setEmailMsg('Please enter a valid email');
              hasError = true;
            }
            if (password.length <= 0) {
              setPasswordMsg('Please enter a password');
              hasError = true;
            }
            logger.log('hasError', hasError);
            if (hasError) {
              return;
            }
            apolloClient
              .query({query: GET_USER_BY_EMAIL, variables: {email}})
              .then((res) => {
                logger.log('GET_USER_BY_EMAIL res', res);
                if (
                  res.data?.user?.password &&
                  md5(password) === res.data.user.password
                ) {
                  navigation.navigate('MainTabNav');
                } else {
                  Alert.alert('Invalid username or password');
                }
              })
              .catch((err) => {
                logger.error('GET_USER_BY_EMAIL err', err);
                Alert.alert('Connection problem');
              });
          }}>
          <View style={styles.longTouchableView}>
            <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
              Log in
            </Text>
          </View>
        </TouchableOpacity>
        <View style={{marginTop: 40}}>
          <Text
            onPress={() => navigation.navigate('ForgotPasswordNav')}
            style={{textDecorationLine: 'underline', color: Colors.blue500}}>
            Forgot your password?
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

export const ForgotPasswordScreen = () => {
  const [email, setEmail] = useState('');
  const [emailMsg, setEmailMsg] = useState('');
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.loginSreen}>
      <View>
        <View
          style={{height: 50, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 24}}>
            Forgot Password
          </Text>
        </View>
        <View style={{marginTop: 10, height: 50}}>
          <TextInput
            autoCapitalize={'none'}
            autoCorrect={false}
            label={'Email'}
            value={email}
            style={{height: 50}}
            onChangeText={(text) => {
              logger.log('email2 is', text);
              setEmail(text);
              setEmailMsg('');
            }}
          />
          <HelperText
            style={{marginTop: -3}}
            type="error"
            visible={emailMsg !== ''}>
            {emailMsg}
          </HelperText>
        </View>

        <TouchableOpacity
          style={[
            styles.longTouchable,
            {
              backgroundColor: Colors.blue500,
              marginTop: 20,
            },
          ]}
          onPress={() => {
            if (email.indexOf('@') <= 0) {
              setEmailMsg('Please enter a valid email');
              return;
            }
            logger.debug('forgotten email is', email);
            authClient
              .mutate({
                mutation: FORGOT_PASSWORD,
                variables: {tokenKey: TOKEN_KEY, email},
              })
              .then((res) => {
                logger.log('FORGOT_PASSWORD res', res);
                if (res.data?.forgotPassword) {
                  navigation.navigate('LoginNav');
                  Alert.alert('Your password was sent to your email address');
                } else {
                  Alert.alert('Invalid email address');
                }
              })
              .catch((err) => {
                logger.error('FORGOT_PASSWORD err', err);
                Alert.alert('Connection problem');
              });
          }}>
          <View style={styles.longTouchableView}>
            <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
              RESET
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  loginSreen: {
    flex: 1,
    alignItems: 'center',
    //justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  text: {fontSize: 32, fontWeight: 'bold', color: 'purple', padding: 10},
  longTouchable: {
    flexDirection: 'row',
    width: SCREEN_WIDTH - 50,
    height: 50,
    alignItems: 'center',
    borderRadius: 25,
  },
  longTouchableIcon: {
    marginLeft: 30,
  },
  longTouchableView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  longTouchableText: {
    marginLeft: -40,
    fontSize: 20,
    color: 'white',
  },
});
