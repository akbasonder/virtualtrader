/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation} from '@react-navigation/native';
import {TextInput, Text, Colors, HelperText} from 'react-native-paper';
import {gql, useMutation} from '@apollo/client';
import md5 from 'md5';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {USER_ID_KEY} from '../../config/constants';
import {atom, useSetRecoilState} from 'recoil';
//import RNPasswordStrengthMeter from 'react-native-password-strength-meter';
//import {BarPasswordStrengthDisplay} from 'react-native-password-strength-meter';

const SCREEN_WIDTH = Dimensions.get('window').width;
const logger = new Logger('Signup');

const CREATE_USER = gql`
  mutation createUser($username: String!, $email: String!, $password: String!) {
    createUser(
      data: {
        username: $username
        email: $email
        password: $password
        loginType: 1
        Account: {
          create: {
            amount: 50000
            averagePrice: 1
            Asset: {connect: {symbol: "USD"}}
          }
        }
      }
    ) {
      id
    }
  }
`;

export const userIdState = atom({
  key: 'userIdState',
  default: 0,
});

export const baseCurrencyState = atom({
  key: 'baseCurrencyState',
  default: 'USD',
});

export const SignupScreen = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [usernameMsg, setUsernameMsg] = useState('');
  const [emailMsg, setEmailMsg] = useState('');
  const [passwordMsg, setPasswordMsg] = useState('');
  const setUserIdState = useSetRecoilState(userIdState);
  const [createUser, {data}] = useMutation(CREATE_USER);
  const navigation = useNavigation();

  return (
    <SafeAreaView style={styles.loginSreen}>
      <View>
        <View
          style={{height: 50, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontWeight: 'bold', fontSize: 24}}> Register</Text>
        </View>
        <Text style={[styles.textHeader]}>PICK A USERNAME</Text>
        <View style={[styles.textView, {marginTop: 7}]}>
          <TextInput
            autoCapitalize={'none'}
            autoCorrect={false}
            label={'username'}
            value={username}
            style={{height: 50}}
            onChangeText={(text) => {
              logger.log('username is', text);
              setUsername(text);
              setUsernameMsg('');
            }}
          />
          <HelperText
            type="error"
            style={{marginTop: -3}}
            visible={usernameMsg !== ''}>
            {usernameMsg}
          </HelperText>
        </View>

        <Text style={[styles.textHeader, {marginTop: 50}]}>
          ACCOUNT INFORMATION
        </Text>
        <View style={[styles.textView, {marginTop: 7}]}>
          <TextInput
            autoCapitalize={'none'}
            autoCorrect={false}
            label={'Email'}
            value={email}
            style={{height: 50}}
            onChangeText={(text) => {
              logger.log('email2 is', text);
              setEmail(text);
              setEmailMsg('');
            }}
          />
          <HelperText
            style={{marginTop: -3}}
            type="error"
            visible={emailMsg !== ''}>
            {emailMsg}
          </HelperText>
        </View>

        <View style={[styles.textView, {marginTop: 20}]}>
          <TextInput
            style={{height: 50}}
            autoCapitalize={'none'}
            autoCorrect={false}
            label={'Password'}
            textContentType={'password'}
            secureTextEntry={true}
            value={password}
            onChangeText={(text) => {
              logger.log('password is', text);
              setPassword(text);
              setPasswordMsg('');
            }}
          />
          <HelperText
            type="error"
            style={{marginTop: -3}}
            visible={passwordMsg !== ''}>
            {passwordMsg}
          </HelperText>
        </View>

        <TouchableOpacity
          style={[
            styles.longTouchable,
            {
              backgroundColor: Colors.blue500,
              marginTop: 30,
            },
          ]}
          onPress={() => {
            let hasError = false;
            if (username.length === 0) {
              setUsernameMsg('Username is needed');
              hasError = true;
            }
            if (email.length === 0) {
              setEmailMsg('Email is needed');
              hasError = true;
            }
            if (password.length === 0) {
              setPasswordMsg('Password is needed');
              hasError = true;
            }
            if (hasError) {
              return;
            }
            createUser({variables: {username, email, password: md5(password)}})
              .then((res) => {
                logger.log('CREATE_USER3 res', res);
                const userId = res.data?.createUser.id;
                setUserIdState(userId);
                AsyncStorage.setItem(USER_ID_KEY, userId.toString());
                navigation.navigate('MainTabNav');
              })
              .catch((err) => {
                logger.error('CREATE_USER err', err.message);
                const msg = err.message;
                //setEmailMsg(err.message.substring(0, 20));
                let netError = true;
                if (
                  msg.indexOf(
                    'Unique constraint failed on the fields: (`username`)',
                  ) >= 0
                ) {
                  setUsernameMsg('Username already exists');
                  netError = false;
                } else {
                  setUsernameMsg('');
                }
                if (
                  msg.indexOf(
                    'Unique constraint failed on the fields: (`email`)',
                  ) >= 0
                ) {
                  setEmailMsg('Email already exists');
                  netError = false;
                } else {
                  setEmailMsg('');
                }

                if (netError) {
                  Alert.alert('Connection problem');
                }
              });
          }}>
          <View style={styles.longTouchableView}>
            <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
              Sign Up
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  loginSreen: {
    flex: 1,
    alignItems: 'center',
    //justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  textHeader: {
    fontWeight: '600',
    fontSize: 12,
    color: Colors.grey800,
  },
  textView: {height: 50},
  longTouchable: {
    flexDirection: 'row',
    width: SCREEN_WIDTH - 50,
    height: 50,
    alignItems: 'center',
    borderRadius: 25,
  },
  longTouchableView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
