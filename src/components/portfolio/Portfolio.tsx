/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
  FlatList,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Text, Colors, ActivityIndicator} from 'react-native-paper';
import {gql, useQuery} from '@apollo/client';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {TimeLineGraph, PERIOD} from '../common/Graph';
import moment from 'moment';
import {createStackNavigator} from '@react-navigation/stack';
import {TradePage} from '../market/Trade';
import {useRecoilValue} from 'recoil';
import {userIdState, baseCurrencyState} from '../auth/Signup';
import {getFirstSymbol} from '../market/Trade';
import {INITIAL_WEALTH} from '../../config/constants';

const logger = new Logger('Portfolio');

//TODO where status='active'
const GET_MY_ACCOUNTS = gql`
  query accounts($userId: Int!) {
    accounts(where: {userId: {equals: $userId}}) {
      id
      amount
      averagePrice
      Asset {
        id
        price
        name
        symbol
        category
        ask
        bid
        capitalName
      }
    }
  }
`;

const GET_MY_TRANSACTIONS = gql`
  query transactions($userId: Int!, $fromDate: DateTime) {
    transactions(
      where: {userId: {equals: $userId}, createdAt: {gte: $fromDate}}
    ) {
      id
      createdAt
      userId
      isSell
      fromAmount
      toAmount
      status
      #FromAsset {
      #id
      #   symbol
      #   name
      #price
      #}
      ToAsset {
        symbol
        name
        price
        ask
        bid
      }
    }
  }
`;

const GET_WEALTH_BY_USER = gql`
  query findManyWealth($userId: Int!, $period: Int, $take: Int!) {
    findManyWealth(
      take: $take
      where: {userId: {equals: $userId}, period: {gte: $period}}
      orderBy: {createdAt: asc}
    ) {
      createdAt
      value
    }
  }
`;

const SCREEN_WIDTH = Dimensions.get('window').width;

const pieChartColors = [
  Colors.yellow500,
  Colors.red300,
  Colors.blue400,
  Colors.purple400,
  Colors.brown400,
  Colors.green800,
];

const composePieChartData = (accountList) => {
  let resultList = [];
  accountList.map((item, index) => {
    const symbol = getFirstSymbol(item.Asset.symbol);
    resultList.push({
      name: symbol, //symbol === 'USD' ? 'USD' : 'USD (' + symbol + ')',
      population: item.amount * item.Asset.price,
      color: pieChartColors[index],
      legendFontColor: Colors.grey600,
      legendFontSize: 14,
    });
  });
  return resultList;
};

const chartConfig = {
  backgroundGradientFrom: '#1E2923',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: '#08130D',
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false, // optional
};

const PortfolioStack = createStackNavigator();
const PortfolioTopTab = createMaterialTopTabNavigator();

export const PortfolioNav = () => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <SafeAreaView style={{flex: 1}}>
        <PortfolioStack.Navigator
          initialRouteName={'PortfolioTopNav'}
          headerMode="none">
          <PortfolioStack.Screen
            name="PortfolioTopNav"
            component={PortfolioTopNav}
            options={{gestureEnabled: false}}
          />
          <PortfolioStack.Screen
            name="PortfolioTradeNav"
            component={TradePage}
          />
        </PortfolioStack.Navigator>
      </SafeAreaView>
    </View>
  );
};

const PortfolioTopNav = () => {
  return (
    <PortfolioTopTab.Navigator>
      <PortfolioTopTab.Screen
        name="totalAssetsNav"
        options={{title: 'Assets'}}
        component={TotalAssetsPage}
        //options={{header: () => null}}
      />
      <PortfolioTopTab.Screen
        name="timeLineDayNav"
        options={{title: 'Day'}}
        component={TimeLinePage}
        initialParams={{period: PERIOD.DAY}}
      />
      <PortfolioTopTab.Screen
        name="timeLineWeekNav"
        options={{title: 'Week'}}
        component={TimeLinePage}
        initialParams={{period: PERIOD.WEEK}}
      />
      <PortfolioTopTab.Screen
        name="timeLineMonthNav"
        options={{title: 'Month'}}
        component={TimeLinePage}
        initialParams={{period: PERIOD.MONTH}}
      />
      <PortfolioTopTab.Screen
        name="timeLineYearNav"
        options={{title: 'Year'}}
        component={TimeLinePage}
        initialParams={{period: PERIOD.YEAR}}
      />
    </PortfolioTopTab.Navigator>
  );
};

const TimeLinePage = () => {
  const route = useRoute();
  const {period} = route.params;
  let fromDate;
  switch (period) {
    case PERIOD.DAY:
      fromDate = moment().subtract(1, 'day').toDate();
      break;
    case PERIOD.WEEK:
      fromDate = moment().subtract(1, 'week').toDate();
      break;
    case PERIOD.MONTH:
      fromDate = moment().subtract(1, 'month').toDate();
      break;
    case PERIOD.MONTH:
      fromDate = moment().subtract(1, 'year').toDate();
      break;
  }

  const testData = [
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,

    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
  ];
  return (
    <View>
      <TimeLine fromDate={fromDate} period={period} />
      <MyTransactions fromDate={fromDate} />
    </View>
  );
};

const TimeLine = ({fromDate, period}) => {
  const userId = useRecoilValue(userIdState);
  let take = 24;
  switch (period) {
    case PERIOD.WEEK:
      take = 8;
      break;
    case PERIOD.MONTH:
      take = 11;
      break;
    case PERIOD.YEAR:
      take = 12;
      break;
  }
  const {loading, data, error} = useQuery(GET_WEALTH_BY_USER, {
    variables: {userId, period: period > PERIOD.DAY ? period : undefined, take},
  });
  if (data) {
    logger.log('timeLine data is', data);
  }
  return loading ? (
    <ShowLoading />
  ) : error ? (
    <ShowError error={error} />
  ) : (
    <TimeLineGraph
      period={period}
      data={wealthToTimeLine(data.findManyWealth, take)}
      height={SCREEN_WIDTH * 0.6}
      backgroundColor={Colors.green200}
    />
  );
};

const wealthToTimeLine = (wealths, take) => {
  logger.debug('wealthToTimeLine started with wealths', wealths);
  let result = []; //[1000,1000,1000,1000,1000,1000,1000,2000,2000,2000,2000,2000];
  wealths.map((item) => {
    logger.log('wealth is', item);
    logger.log('wealth value is', parseFloat(item.value));
    //result.push(parseFloat(item.value));
    result.splice(0, 0, parseFloat(item.value));
  });
  while (result.length < take) {
    //result.push(INITIAL_WEALTH);
    result.splice(0, 0, parseFloat(INITIAL_WEALTH));
  }
  return result;
};

const TotalAssetsPage = () => {
  //
  const userId = useRecoilValue(userIdState);
  const navigation = useNavigation();
  const baseCurrency = useRecoilValue(baseCurrencyState);
  const {loading, error, data} = useQuery(GET_MY_ACCOUNTS, {
    variables: {userId},
  });
  if (data) {
    logger.debug('asset data', data);
  }
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
      {loading ? (
        <ShowLoading />
      ) : data ? (
        <View>
          <PieChart
            data={composePieChartData(data.accounts)}
            width={SCREEN_WIDTH}
            height={SCREEN_WIDTH * 0.6}
            chartConfig={chartConfig}
            accessor={'population'}
            backgroundColor={'transparent'}
            paddingLeft={'0'}
            //center={[10, 50]}
            absolute
            style={{backgroundColor: Colors.green200}}
          />
          {renderTransactionHeader(true)}
          <FlatList
            data={data.accounts}
            renderItem={({item}) =>
              renderTransaction(item, true, baseCurrency, navigation)
            }
            //ListHeaderComponent={() => renderTransactionHeader(true)}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
      ) : (
        error && <ShowError error={error} />
      )}
    </View>
  );
};

type TranscationListType = {
  item: TranscationType;
  index: number;
};

type TranscationType = {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  currentUnitPrice: number;
  purchasedUnitPrice: number;
  //changeTotal: number;
  //changePercantage: number;
};

const transactionList: [TranscationType] = [
  {
    id: 1,
    symbol: 'XAU',
    name: 'Gold',
    amount: 100,
    currentUnitPrice: 75,
    purchasedUnitPrice: 70,
  },
  {
    id: 2,
    symbol: 'GOOGL',
    name: 'Alphabet',
    amount: 60,
    currentUnitPrice: 45,
    purchasedUnitPrice: 50,
  },
  {
    id: 3,
    symbol: 'APPL',
    name: 'Apple',
    amount: 80,
    currentUnitPrice: 85,
    purchasedUnitPrice: 78,
  },
  {
    id: 4,
    symbol: 'XBT',
    name: 'Bitcoin',
    amount: 20,
    currentUnitPrice: 65,
    purchasedUnitPrice: 60,
  },
  {
    id: 5,
    symbol: 'EUR',
    name: 'Euro',
    amount: 120,
    currentUnitPrice: 115,
    purchasedUnitPrice: 100,
  },
  {
    id: 56,
    symbol: 'USD',
    name: 'US Dolar',
    amount: 40,
    currentUnitPrice: 100,
    purchasedUnitPrice: 100,
  },
];

const showName = (name: string | undefined): string => {
  if (!name) {
    return '';
  }
  const index1 = name.indexOf(' ');
  if (index1 > 0) {
    const index2 = name.substring(index1 + 1).indexOf(' ');
    if (index2 > 0) {
      return name.substring(0, index1 + index2 + 1);
    }
  }
  return name;
};

const renderTransaction = (
  item,
  isAsset = false,
  baseCurrency = 'USD',
  navigation = undefined,
) => {
  if (isAsset) {
    logger.log('renderTransaction1 item is', item);
  } else {
    logger.log('renderTransaction2 item is', item);
  }

  const {
    name,
    amount,
    currentUnitPrice,
    purchasedUnitPrice,
    symbol,
  } = navigation ? accountToAsset(item) : transactionToAsset(item);
  const isProfit = currentUnitPrice >= purchasedUnitPrice;
  const numberColor = isProfit ? Colors.green600 : Colors.red400;
  const sign = isProfit ? '+' : '';
  logger.log('transaction symnbol is', symbol);
  const isBaseCurrency = baseCurrency === symbol;
  return (
    <View
      style={{
        borderBottomColor: Colors.grey400,
        borderBottomWidth: 1,
      }}>
      <View
        style={{
          height: 60,
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          marginLeft: 5,
          marginRight: 5,
        }}>
        <View style={styles.rowValue}>
          <Text style={styles.textValue}>{showName(name)}</Text>
          <Text style={[styles.textValue, {color: numberColor}]}>
            {amount} {isBaseCurrency ? '' : 'x '.concat(currentUnitPrice)}
          </Text>
        </View>
        <View style={styles.rowValue}>
          <Text style={styles.textValue}>{amount * purchasedUnitPrice}</Text>
          <Text style={[styles.textValue, {color: numberColor}]}>
            {amount * currentUnitPrice}
          </Text>
        </View>
        <View style={styles.rowValue}>
          <Text style={[styles.textValue, {color: numberColor}]}>
            {sign}
            {Math.round((currentUnitPrice / purchasedUnitPrice - 1) * 10000) /
              100}
            %
          </Text>
          <Text style={[styles.textValue, {color: numberColor}]}>
            {sign}
            {amount * (currentUnitPrice - purchasedUnitPrice)}
          </Text>
        </View>
        <View style={styles.rowValue}>
          {isAsset ? (
            isBaseCurrency ? (
              <View
                style={{
                  width: '100%',
                  height: 60,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={styles.baseCurrencyText}>Base</Text>
                <Text style={styles.baseCurrencyText}>Currency</Text>
              </View>
            ) : (
              <View style={{width: '100%', height: 60, alignItems: 'center'}}>
                <TouchableOpacity
                  style={styles.tradeButton}
                  onPress={() =>
                    navigation.navigate('PortfolioTradeNav', {
                      isSell: true,
                      item: item.Asset,
                    })
                  }>
                  <Text
                    style={{
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: '600',
                    }}>
                    Sell
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.tradeButton}
                  onPress={() =>
                    navigation.navigate('PortfolioTradeNav', {
                      isSell: false,
                      item: item.Asset,
                    })
                  }>
                  <Text
                    style={{
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: '600',
                    }}>
                    Buy
                  </Text>
                </TouchableOpacity>
              </View>
            )
          ) : (
            <Text style={styles.textValue}>
              {moment(item.createdAt).format('DD MMM YYYY')}
            </Text>
          )}
        </View>
      </View>
    </View>
  );
};

const renderTransactionHeader = (isAsset = false) => {
  return (
    <View
      style={{
        height: 30,
        backgroundColor: Colors.grey400,
      }}>
      <View
        style={{
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
          marginLeft: 20,
          marginRight: 20,
        }}>
        <View style={styles.headerTitle}>
          <Text style={styles.textHeader}> Amount</Text>
        </View>
        <View style={styles.headerTitle}>
          <Text style={styles.textHeader}> Value </Text>
        </View>
        <View style={styles.headerTitle}>
          <Text style={styles.textHeader}> Change</Text>
        </View>

        <View style={styles.headerTitle}>
          <Text style={styles.textHeader}> {isAsset ? 'Trade' : 'Date'}</Text>
        </View>
      </View>
    </View>
  );
};

export const ShowLoading = () => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator animating={true} size={'large'}></ActivityIndicator>
    </View>
  );
};

export const ShowError = ({error}) => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Exception occured</Text>
      <Text>{error.message}</Text>
    </View>
  );
};
const MyTransactions = ({fromDate}) => {
  const userId = useRecoilValue(userIdState);
  logger.log('userId is', userId);
  const {loading, data, error} = useQuery(GET_MY_TRANSACTIONS, {
    variables: {userId, fromDate},
  });
  if (data) {
    logger.log('transactions data is', data);
  }
  return (
    <View>
      {loading ? (
        <ShowLoading />
      ) : error ? (
        <ShowError error={error} />
      ) : (
        <View>
          {renderTransactionHeader(false)}
          <FlatList
            data={data.transactions}
            renderItem={({item}) => renderTransaction(item, false)}
            //ListHeaderComponent={() => renderTransactionHeader(false)}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
      )}
    </View>
  );
};

const accountToAsset = (item) => {
  logger.debug('accountToAsset item is', item);
  logger.debug('accountToAsset item.Asset is', item.Asset);
  const result = {
    id: item.id,
    symbol: item.Asset.symbol,
    name: item.Asset.name,
    amount: item.amount,
    purchasedUnitPrice: item.averagePrice,
    currentUnitPrice: item.Asset.price,
  };
  logger.debug('accountToAsset result is', result);
  return result;
};

const transactionToAsset = (item) => {
  logger.debug('transactionToAsset item is', item);
  const result = {
    id: item.id,
    symbol: item.ToAsset.symbol,
    name: item.ToAsset.name,
    amount: item.toAmount,
    purchasedUnitPrice: item.toAmount / item.fromAmount,
    currentUnitPrice: item.isSell ? item.ToAsset.bid : item.ToAsset.ask,
  };
  logger.debug('transactionToAsset result is', result);
  return result;
};

const styles = StyleSheet.create({
  textHeader: {
    fontSize: 18,
    fontWeight: '500',
  },
  textValue: {
    fontSize: 13,
    fontWeight: '400',
    textAlign: 'center',
  },
  headerTitle: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
  },
  rowValue: {
    width: '25%',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  rowTrade: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
  },
  tradeButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.blue600,
    height: 25,
    borderRadius: 13,
    width: '80%',
    marginTop: 3,
  },
  baseCurrencyText: {
    color: Colors.grey900,
    fontSize: 16,
    fontWeight: '600',
  },
});
