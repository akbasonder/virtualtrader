/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
  FlatList,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Text, Colors} from 'react-native-paper';
import {gql} from '@apollo/client';
import {LineChart} from 'react-native-chart-kit';
import moment from 'moment';

const logger = new Logger('TimeLineGraph');

const SCREEN_WIDTH = Dimensions.get('window').width;

export enum PERIOD {
  'DAY' = 1,
  'WEEK' = 2,
  'MONTH' = 3,
  'YEAR' = 4,
}

export const TimeLineGraph = ({period, data, height, backgroundColor}) => {
  let labels = [];
  if (period === PERIOD.DAY) {
    for (let i = 12; i >= 1; i--) {
      labels.push(
        moment()
          .subtract(i * 2, 'hours')
          .format('HH'),
      );
    }
  }
  if (period === PERIOD.WEEK) {
    for (let i = 7; i >= 1; i--) {
      labels.push(moment().subtract(i, 'days').date());
    }
  }
  if (period === PERIOD.MONTH) {
    for (let i = 9; i >= 0; i--) {
      labels.push(
        moment()
          .subtract(i * 3 + 1, 'days')
          .date(),
      );
    }
  }
  if (period === PERIOD.YEAR) {
    for (let i = 12; i >= 1; i--) {
      labels.push(moment().subtract(i, 'months').format('MMM'));
    }
  }

  return (
    <View style={{backgroundColor: backgroundColor}}>
      <LineChart
        data={{
          labels, //: ['January', 'February', 'March', 'April', 'May', 'June'],
          datasets: [
            {
              data,
            },
          ],
        }}
        width={SCREEN_WIDTH} // from react-native
        height={height}
        yAxisLabel="$"
        //yAxisSuffix="k"
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          //backgroundColor: 'red', // '#e26a00',
          backgroundGradientFrom: backgroundColor, //'#fb8c00',
          backgroundGradientTo: backgroundColor, //'#ffa726',
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => Colors.blue600,
          labelColor: (opacity = 1) => Colors.brown800,
          //color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          //labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: '6',
            strokeWidth: '2',
            stroke: '#ffa726',
          },
        }}
        bezier
        style={
          {
            //marginVertical: 8,
            //borderRadius: 16,
          }
        }
      />
    </View>
  );
};
