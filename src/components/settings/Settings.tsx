import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  Image,
} from 'react-native';
import I18n from '../../config/language';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {USERNAME_KEY, PASSWORD_KEY, VERSION} from '../../config/constants';
import {createStackNavigator} from '@react-navigation/stack';
import {ChangeLanguage} from './ChangeLanguage';
import {gql, useQuery} from '@apollo/client';
import {Logger} from '../../utils/Logger';
import {useRecoilValue} from 'recoil';
import {usernameState} from '../auth/Login';
import {Switch} from 'react-native-paper';

const SCREEN_WIDTH = Dimensions.get('window').width;
const logger = new Logger('Settings');
const SettingsStack = createStackNavigator();

export const Settings = () => {
  return (
    <SettingsStack.Navigator>
      <SettingsStack.Screen
        name="SettingsMainNav"
        component={SettingsMain}
        options={{header: () => null}}
      />
      <SettingsStack.Screen
        name="ChangeLanguageNav"
        component={ChangeLanguage}
        options={{
          headerBackTitle: I18n.t('Settings'),
          title: I18n.t('Change Language'),
        }}
      />
    </SettingsStack.Navigator>
  );
};

const SettingsMain = () => {
  const navigation = useNavigation();

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <View
        style={{
          height: 50,
          alignItems: 'center',
          justifyContent: 'flex-end',
          borderBottomColor: 'rgb(128,128,128)',
          borderBottomWidth: 1,
        }}>
        <Text style={{fontSize: 16, fontWeight: '600'}}>
          {I18n.t('Settings')}
        </Text>
        <Text style={{marginBottom: 5, fontSize: 14}}>
          {I18n.t('Version')} {VERSION}
        </Text>
      </View>
      <View
        style={{
          backgroundColor: 'white',
          flex: 1,
          justifyContent: 'space-between',
        }}>
        <View>
          <TouchableOpacity
            style={styles.optionsButton}
            onPress={() => navigation.navigate('ChangeLanguageNav')}>
            <Text style={styles.optionText}>Language</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.optionText}>English</Text>
              <MaterialIcons
                name="arrow-forward-ios"
                color={'rgb(64,64,64)'}
                size={22}
                style={{marginRight: 10}}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.optionsButton}
            onPress={() => navigation.navigate('ChangeLanguageNav')}>
            <Text style={styles.optionText}>Base Currency</Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.optionText}>USD</Text>
              <MaterialIcons
                name="arrow-forward-ios"
                color={'rgb(64,64,64)'}
                size={22}
                style={{marginRight: 10}}
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            style={styles.logoutButton}
            onPress={async () => {
              await AsyncStorage.removeItem(USERNAME_KEY);
              await AsyncStorage.removeItem(PASSWORD_KEY);
              navigation.navigate('AuthNav');
            }}>
            <Text style={{fontSize: 16, fontWeight: '600'}}>
              {I18n.t('Logout')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  optionsButton: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(128,128,128)',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  optionText: {
    fontSize: 16,
    fontWeight: '400',
    marginLeft: 25,
  },
  logoutButton: {
    backgroundColor: '#999caa',
    width: 300,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 40,
    borderRadius: 25,
  },
});
