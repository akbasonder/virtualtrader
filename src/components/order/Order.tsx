/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
//import { SafeAreaView } from 'react-native-safe-area-context';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';
import {Logger} from '../../utils/Logger';
import RadioForm from 'react-native-simple-radio-button';
import {Text, Colors, DataTable} from 'react-native-paper';
import DateTimePicker from '@react-native-community/datetimepicker';
//import moment from 'moment';
// TODO: Date can be added later

const AuthStack = createStackNavigator();
const SCREEN_WIDTH = Dimensions.get('window').width;
const logger = new Logger('Order');

const TRANSACTION_TYPES = [
  {label: 'Buy               ', value: 0},
  {label: 'Sell          ', value: 1},
  {label: 'All  ', value: 2},
];

const ORDER_STATUS = [
  {label: 'Performed    ', value: 0},
  {label: 'Waiting   ', value: 1},
  {label: 'All  ', value: 2},
];

const SHOW_DATE = [
  {label: 'Any    ', value: false},
  {label: 'Date :  ', value: true},
];

export const OrderScreen = () => {
  const [transactionType, setTransactionType] = useState(2);
  const [orderStatus, setOrderStatus] = useState(2);
  const [fromDate, setFromDate] = useState(new Date());
  const [fromDateFlag, setFromDateFlag] = useState(false);
  const onChangeFrom = (event, selectedDate) => {
    const currentDate = selectedDate || new Date();
    setFromDate(currentDate);
  };
  const [toDate, setToDate] = useState(new Date());
  const [toDateFlag, setToDateFlag] = useState(false);
  const onChangeTo = (event, selectedDate) => {
    const currentDate = selectedDate || new Date();
    setToDate(currentDate);
  };
  const navigation = useNavigation();
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{borderTopColor: Colors.grey400, borderTopWidth: 1}} />
      {/*<View style={styles.pageHeader}>
        <Text style={{fontSize: 18, fontWeight: '500'}}>Filters</Text>
  </View>*/}
      <View style={styles.filterRow}>
        <Text style={styles.rowTextHeader}>Type:</Text>
        <View style={{}}>
          <RadioForm
            radio_props={TRANSACTION_TYPES}
            initial={1}
            formHorizontal={true}
            onPress={(value: number) => {
              setTransactionType(value);
            }}
          />
        </View>
      </View>

      <View style={styles.filterRow}>
        <Text style={styles.rowTextHeader}>Status:</Text>
        <View style={{}}>
          <RadioForm
            radio_props={ORDER_STATUS}
            initial={2}
            formHorizontal={true}
            onPress={(value: number) => {
              setOrderStatus(value);
            }}
          />
        </View>
      </View>

      <View style={styles.filterRow}>
        <Text style={styles.rowTextHeader}>From:</Text>
        <View style={{}}>
          <RadioForm
            radio_props={SHOW_DATE}
            initial={false}
            formHorizontal={true}
            onPress={(value: boolean) => {
              setFromDateFlag(value);
            }}
          />
        </View>
        {fromDateFlag && (
          <DateTimePicker
            value={fromDate}
            maximumDate={new Date()}
            mode={'date'}
            display="default"
            onChange={onChangeFrom}
            style={{height: 50, flex: 1, marginLeft: 10}}
          />
        )}
      </View>

      <View style={styles.filterRow}>
        <Text style={styles.rowTextHeader}>To:</Text>
        <View style={{}}>
          <RadioForm
            radio_props={SHOW_DATE}
            initial={false}
            formHorizontal={true}
            onPress={(value: boolean) => {
              setToDateFlag(value);
            }}
          />
        </View>
        {toDateFlag && (
          <DateTimePicker
            value={toDate}
            mode={'date'}
            display="default"
            onChange={onChangeTo}
            maximumDate={fromDate}
            style={{height: 50, flex: 1, marginLeft: 10}}
          />
        )}
      </View>

    </SafeAreaView>
  );
};

/*export const PortfolioTopNav = () => {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <SafeAreaView style={{flex: 1}}>
          <PortfolioTopStack.Navigator>
            <PortfolioTopStack.Screen
              name="totalAssetsNav"
              options={{title: 'Assets'}}
              component={TotalAssetsPage}
              //options={{header: () => null}}
            />
            <PortfolioTopStack.Screen
              name="timeLineWeekNav"
              options={{title: 'Week'}}
              component={TimeLinePage}
              initialParams={{period: PERIOD.WEEK}}
            />
            <PortfolioTopStack.Screen
              name="timeLineMonthNav"
              options={{title: 'Month'}}
              component={TimeLinePage}
              initialParams={{period: PERIOD.MONTH}}
            />
            <PortfolioTopStack.Screen
              name="timeLineYearNav"
              options={{title: 'Year'}}
              component={TimeLinePage}
              initialParams={{period: PERIOD.YEAR}}
            />
          </PortfolioTopStack.Navigator>
        </SafeAreaView>
      </View>
    );
  };*/

const styles = StyleSheet.create({
  pageHeader: {
    borderColor: Colors.grey400,
    borderWidth: 1,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterRow: {
    borderBottomColor: Colors.grey400,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
  },
  rowTextHeader: {fontSize: 16, marginBottom: 2, marginLeft: 15, width: 80},
});
