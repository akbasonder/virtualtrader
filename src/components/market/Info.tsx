/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
  Pressable,
  ScrollView,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Text, Colors} from 'react-native-paper';
import {TimeLineGraph, PERIOD} from '../common/Graph';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const logger = new Logger('Info');

const SCREEN_WIDTH = Dimensions.get('window').width;

export const InfoPage = () => {
  const navigation = useNavigation();
  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <View
        style={{
          flexDirection: 'row',
          //backgroundColor: Colors.blue100,
          alignItems: 'center',
          justifyContent: 'space-between',
          borderBottomColor: Colors.grey400,
          borderBottomWidth: 1,
        }}>
        <Pressable
          style={{
            height: 50,
            width: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <MaterialIcons name="arrow-back-ios" size={30} />
        </Pressable>

        <Text> Ripple (XRP) </Text>
        <Pressable style={styles.starIcon} onPress={() => Alert.alert('star')}>
          <MaterialIcons
            size={30}
            name={'star'}
            color={Colors.yellow800}
            //style={{marginLeft: 0, marginTop: -5}}
          />
        </Pressable>
      </View>
      <Detail
        description={'Last Refresh'}
        name={'TIME'}
        value={'09 Mar 2021 14:11'}
      />
      <Detail
        description={'Average of Bid and Ask'}
        name={'MID'}
        value={'50,97'}
      />
      <Detail description={'Bid, Sell Price'} name={'BID'} value={'50,87'} />
      <Detail description={'Ask, Buy Price'} name={'ASK'} value={'50,17'} />
      <Detail
        description={'Difference between Bid and Ask'}
        name={'SPREAD'}
        value={'0,200 = 0,4 %'}
      />
      <Detail
        description={'Relative Change since Yesterday'}
        name={'%'}
        value={'+2,00 %'}
        valueColor={Colors.green800}
      />
      <Detail
        description={'Absolute Change since Yesterday'}
        name={'+/-'}
        value={'- 0.7'}
        valueColor={Colors.red800}
      />
    </View>
  );
};

type DetailType = {
  description: string;
  value: string;
  name: string;
  valueColor: string;
};

const Detail = ({
  description,
  value,
  name,
  valueColor = Colors.black,
}: DetailType) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        height: 50,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: Colors.grey400,
        borderBottomWidth: 1,
      }}>
      <View style={{marginLeft: 25}}>
        <Text style={{fontSize: 16, color: valueColor}}>{value}</Text>
        <Text style={{fontSize: 14, color: Colors.grey700}}>{description}</Text>
      </View>
      <Text
        style={{
          marginRight: 25,
          fontSize: 20,
          fontWeight: '600',
          color: Colors.grey600,
        }}>
        {name}
      </Text>
    </View>
  );
};

type TranscationListType = {
  item: TranscationType;
  index: number;
};

type TranscationType = {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  currentUnitPrice: number;
  purchasedUnitPrice: number;
  //changeTotal: number;
  //changePercantage: number;
};

const styles = StyleSheet.create({
  headerInfo: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 30,
  },
  starIcon: {
    borderColor: Colors.grey500,
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedTime: {
    borderRadius: 15,
    width: 60,
    backgroundColor: Colors.grey400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  notSelectedTime: {
    borderRadius: 15,
    width: 60,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: Colors.grey800,
  },
  actionButton: {
    width: 150,
    marginTop: 20,
    backgroundColor: Colors.blue600,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 25,
  },
  actionText: {
    color: Colors.white,
    fontWeight: '500',
    fontSize: 16,
  },
});
