/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Alert,
  Pressable,
  FlatList,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Text, Colors, Surface, ActivityIndicator} from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IonIcons from 'react-native-vector-icons/Ionicons';
//import {createState, useState} from '@hookstate/core';
import {useRecoilState, useRecoilValue, useSetRecoilState} from 'recoil';
import {GET_ASSETS, searchTextState} from './MainPage';
import {gql, useQuery} from '@apollo/client';
import { ShowError, ShowLoading } from '../portfolio/Portfolio';

const logger = new Logger('Market');

type MarketItemType = {
  id: number;
  name: string;
  symbol: string;
  priority: number;
  rank: number;
  open: number;
  high: number;
  low: number;
  changeNominal: number;
  changePercentage: number;
  tm: string;
};



// orderBy: {priority: 'asc'}) {     id    }
export const MarketPage = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const searchText = useRecoilValue(searchTextState);
  const category = route.params?.name.toUpperCase();
  logger.log('category is', category);
  const {loading, error, data} = useQuery(GET_ASSETS, {
    variables: {category},
  });
  if (data) {
    logger.debug('assets are', data.assets);
  }

  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <View
        style={{
          flexDirection: 'row',
          borderBottomColor: Colors.grey400,
          borderBottomWidth: 1,
        }}>
        <Pressable
          style={{
            height: 50,
            width: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <MaterialIcons name="arrow-back-ios" size={30} />
        </Pressable>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{marginLeft: -70, fontSize: 16, fontWeight: '500'}}>
            {category}
          </Text>
        </View>
      </View>
      {loading ? (
        <ShowLoading/>
      ) : error ? (
        <ShowError error={error} />
      ) : (
        <View>
          <FlatList
            data={data.assets}
            //renderItem={(item)=><MarketItem id={item.item.id}/>}
            //renderItem={MarketItem}
            renderItem={({item}) => <MarketItem item={item} />}
            keyExtractor={(item) => item.id.toString()}
            contentInset={{right: 0, top: 0, left: 0, bottom: 50}}
          />
          {/*<Text>{data.assets[0].id}</Text>*/}
        </View>
      )}
    </View>
  );
};
//<MarketItem id={1} />

export const MarketItem = ({item}: any) => {
  const navigation = useNavigation();
  const setSearchText = useSetRecoilState(searchTextState);
  logger.debug('MarketItem item is222', item);
  const price = parseFloat(item.price).toFixed(4);
  const bid = item.bid
    ? parseFloat(item.bid).toFixed(4)
    : (price * 0.995).toFixed(4);
  const ask = item.ask
    ? parseFloat(item.ask).toFixed(4)
    : (price * 1.005).toFixed(4);

  return (
    <Surface
      style={{
        //height: 80,
        borderColor: Colors.grey400,
        borderWidth: 1,
        margin: 5,
        backgroundColor: Colors.grey100,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
      }}>
      <Pressable
        style={styles.actionIcon}
        onPress={() => {
          setSearchText('');
          navigation.navigate('InfoNav', {assetId: item.id});
        }}>
        <MaterialCommunityIcons
          size={30}
          name={'information-variant'}
          color={Colors.green500}
          //style={{marginLeft: 0, marginTop: -5}}
        />
      </Pressable>

      <View style={{}}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={styles.valueText}>{item.name}</Text>
          {/*United Airlines Holdings (UAL)*/}
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => {
              setSearchText('');
              navigation.navigate('TradeNav', {isSell: true, item});
            }}
            style={styles.actionButton}>
            <Text style={styles.actionText}>Sell</Text>
            <Text style={styles.actionText}>{bid}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionButton}
            onPress={() => {
              setSearchText('');
              navigation.navigate('TradeNav', {isSell: false, item});
            }}>
            <Text style={styles.actionText}>Buy</Text>
            <Text style={styles.actionText}>{ask}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          alignItems: 'center',
          flex: 1,
          justifyContent: 'space-around',
          //height: 70,
        }}>
        <Text style={styles.valueText}>{price}</Text>
        <Text style={styles.valueText}>45</Text>
        <Text style={styles.valueText}>%1.3</Text>
      </View>
      <Pressable
        style={styles.actionIcon}
        onPress={() => {
          setSearchText('');
          navigation.navigate('StatisticsNav');
        }}>
        <IonIcons
          size={30}
          name={'stats-chart'}
          color={Colors.red500}
          //style={{marginLeft: 10, marginTop: -5}}
        />
      </Pressable>
      {/*<Text style={styles.textPercentage}>6,23%</Text>*/}
    </Surface>
  );
};

const styles = StyleSheet.create({
  actionIcon: {
    borderColor: Colors.grey500,
    height: 40,
    borderWidth: 1,
    borderRadius: 20,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    //margin: 10,
  },
  actionButton: {
    width: 100,
    marginTop: 10,
    marginLeft: 10,
    backgroundColor: Colors.blue600,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 25,
  },
  actionText: {
    color: Colors.white,
    fontWeight: '500',
    fontSize: 14,
  },
  valueText: {
    fontSize: 15,
    fontWeight: '400',
    marginTop: 5,
  },
  textPercentage: {
    flex: 1,
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'right',
    color: Colors.green600,
    marginTop: 20,
    marginRight: 20,
  },
  textCenterView: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 105,
  },
});
