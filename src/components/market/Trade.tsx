import React, {useState} from 'react';
import {
  TextInput,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
  FlatList,
  Pressable,
  ScrollView,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Text, Colors} from 'react-native-paper';
import {gql, useMutation} from '@apollo/client';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IonIcons from 'react-native-vector-icons/Ionicons';
import {createStackNavigator} from '@react-navigation/stack';
//import {createState, useState} from '@hookstate/core';
import {atom, useRecoilState, useRecoilValue} from 'recoil';
import {Switch} from 'react-native-paper';
import {authClient} from '../../service/GraphQLClient';
import {TOKEN_KEY} from '../../config/constants';
import {baseCurrencyState, userIdState} from '../auth/Signup';

const logger = new Logger('Trade');

const TRADE = gql`
  mutation trade(
    $tokenKey: String!
    $fromAmount: Float!
    $fromAssetSymbol: String!
    $toAmount: Float!
    $toAssetSymbol: String!
    $isSell: Boolean!
    $userId: Int!
  ) {
    trade(
      tokenKey: $tokenKey
      fromAmount: $fromAmount
      fromAssetSymbol: $fromAssetSymbol
      toAmount: $toAmount
      toAssetSymbol: $toAssetSymbol
      isSell: $isSell
      userId: $userId
    ) {
      code
    }
  }
`;

const showShortName = (name: string) => {
  const index = name.indexOf(' US Dollar');
  if (index > 0) {
    return name.substring(0, index);
  }
  return name;
};

export const TradePage = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const isSell = route.params?.isSell;
  const tradeItem = route.params?.item;
  logger.log('TradePage item is', tradeItem);
  const rate = isSell ? parseFloat(tradeItem.bid) : parseFloat(tradeItem.ask);
  const operation = isSell ? 'Sell' : 'Buy';
  const [amountAsset, setAmountAsset] = useState('0');
  const [amountCurrency, setAmountCurrency] = useState('0');
  const [closeAtProfit, setCloseAtProfit] = useState('');
  const [isCloseAtProfit, setIsCloseAtProfit] = useState(false);
  const onCloseAtProfitSwitch = () => {
    setIsCloseAtProfit(!isCloseAtProfit);
  };
  const [closeAtLoss, setCloseAtLoss] = useState('');
  const [isCloseAtLoss, setIsCloseAtLoss] = useState(false);
  const onCloseAtLossSwitch = () => {
    setIsCloseAtLoss(!isCloseAtLoss);
  };
  const [whenRate, setWhenRate] = useState('');
  const [isWhenRate, setIsWhenRate] = useState(false);
  const onWhenRateSwitch = () => {
    setIsWhenRate(!isWhenRate);
  };
  const baseCurrency = useRecoilValue(baseCurrencyState);
  const userId = useRecoilValue(userIdState);

  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <View style={{flexDirection: 'row', backgroundColor: Colors.blue100}}>
        <Pressable
          style={{
            height: 50,
            width: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <MaterialIcons name="arrow-back-ios" size={30} />
        </Pressable>
        <View style={styles.headerInfo}>
          <Text>
            {' '}
            {operation} {showShortName(tradeItem.name)}{' '}
          </Text>
          <Text> Current Rate: {rate.toFixed(4)} </Text>
        </View>
      </View>
      <ScrollView>
        <View
          style={{
            marginTop: 10,
            marginBottom: 20,
            flexDirection: 'row',
            marginLeft: '5%',
            marginRight: '5%',
          }}>
          <View style={{width: '50%', alignItems: 'center'}}>
            <Text style={{}}>Amount ({getFirstSymbol(tradeItem.symbol)})</Text>
            <TextInput
              style={styles.amountTextInput}
              keyboardType={'numeric'}
              placeholder={'0'}
              onChangeText={(text) => {
                const val = getNumericVal(text);
                if (!val.endsWith('.')) {
                  const numberVal: number = parseFloat(val);
                  setAmountAsset(numberVal.toString());
                  setAmountCurrency(numberVal === 0 ? 0 :
                    (parseFloat(val) * rate).toFixed(4).toString(),
                  );
                } else {
                  setAmountAsset(val);
                }
                //setAmountCurrency((val * rate).toFixed(4));
              }}
              value={amountAsset.toString()}></TextInput>
          </View>
          <View style={{width: '50%', alignItems: 'center', marginLeft: 10}}>
            <Text style={{}}>Amount ({baseCurrency})</Text>
            <TextInput
              style={styles.amountTextInput}
              keyboardType={'numeric'}
              placeholder={'0'}
              onChangeText={(text) => {
                const val = getNumericVal(text);

                if (!val.endsWith('.')) {
                  const numberVal: number = parseFloat(val);
                  setAmountCurrency(numberVal.toString());
                  setAmountAsset(numberVal === 0 ? 0 :
                    (parseFloat(val) / rate).toFixed(4).toString(),
                  );
                } else {
                  setAmountCurrency(val);
                }
              }}
              value={amountCurrency.toString()}
            />
          </View>
        </View>

        <View style={styles.actionRow}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Pressable
              style={styles.infoIcon}
              onPress={() => Alert.alert('bell')}>
              <MaterialCommunityIcons
                size={30}
                name={'information-variant'}
                //name={'information-outline'}
                color={Colors.grey500}
              />
            </Pressable>
            <Text style={styles.actionText}>Close at Profit</Text>
          </View>
          <Switch
            value={isCloseAtProfit}
            onValueChange={onCloseAtProfitSwitch}
            style={{marginRight: 10}}
          />
        </View>
        {isCloseAtProfit && (
          <View style={styles.amountTextView}>
            <View style={{width: '50%', marginLeft: '10%'}}>
              <TextInput
                style={styles.amountTextInput}
                keyboardType={'numeric'}
                placeholder={'0'}
                onChangeText={(text) => setCloseAtProfit(text)}
                value={closeAtProfit}
              />
            </View>
            <Text> Profit: 87.23</Text>
            <Text> | </Text>
            <Text> 0.7%</Text>
          </View>
        )}

        <View style={styles.actionRow}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Pressable
              style={styles.infoIcon}
              onPress={() => Alert.alert('bell')}>
              <MaterialCommunityIcons
                size={30}
                name={'information-variant'}
                //name={'information-outline'}
                color={Colors.grey500}
              />
            </Pressable>
            <Text style={styles.actionText}>Close at Loss</Text>
          </View>
          <Switch
            value={isCloseAtLoss}
            onValueChange={onCloseAtLossSwitch}
            style={{marginRight: 10}}
          />
        </View>
        {isCloseAtLoss && (
          <View style={styles.amountTextView}>
            <View style={{width: '50%', marginLeft: '10%'}}>
              <TextInput
                style={styles.amountTextInput}
                keyboardType={'numeric'}
                placeholder={'0'}
                onChangeText={(text) => setCloseAtLoss(text)}
                value={closeAtLoss}
              />
            </View>
            <Text> Profit: 87.23</Text>
            <Text> | </Text>
            <Text> 0.7%</Text>
          </View>
        )}

        <View style={styles.actionRow}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Pressable
              style={styles.infoIcon}
              onPress={() => Alert.alert('bell')}>
              <MaterialCommunityIcons
                size={30}
                name={'information-variant'}
                //name={'information-outline'}
                color={Colors.grey500}
              />
            </Pressable>
            <Text style={styles.actionText}>{operation} when rate is</Text>
          </View>
          <Switch
            value={isWhenRate}
            onValueChange={onWhenRateSwitch}
            style={{marginRight: 10}}
          />
        </View>
        {isWhenRate && (
          <View style={styles.amountTextView}>
            <View style={{width: '50%', marginLeft: '10%'}}>
              <TextInput
                style={styles.amountTextInput}
                keyboardType={'numeric'}
                placeholder={'0'}
                onChangeText={(text) => setWhenRate(text)}
                value={whenRate}
              />
            </View>
            <Text> Profit: 87.23</Text>
            <Text> | </Text>
            <Text> 0.7%</Text>
          </View>
        )}
        <View
          style={{borderBottomColor: Colors.grey400, borderBottomWidth: 1}}
        />
      </ScrollView>
      <View
        style={{
          height: 60,
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 10,
        }}>
        <TouchableOpacity
          style={styles.actionButton}
          onPress={() => {
            //const fromAmount = parseFloat(amountCurrency);
            //const toAmount = parseFloat(amountAsset);
            const fromAssetSymbol = baseCurrency;
            const toAssetSymbol = tradeItem.symbol;
            if (
              amountCurrency.endsWith('/') ||
              amountAsset.endsWith('/') ||
              parseFloat(amountCurrency) < 0.01
            ) {
              Alert.alert('Please enter a value');
              return;
            }

            authClient
              .mutate({
                mutation: TRADE,
                variables: {
                  tokenKey: TOKEN_KEY,
                  fromAmount: parseFloat(amountCurrency),
                  fromAssetSymbol,
                  toAmount: parseFloat(amountAsset),
                  toAssetSymbol,
                  isSell,
                  userId,
                },
              })
              .then((res) => {
                logger.log('trade operation result is', res);
              })
              .catch((err) => {
                logger.error('trading error is', err);
                Alert.alert('Connection problem');
              });
          }}>
          <Text style={{fontSize: 18, fontWeight: 'bold', color: Colors.white}}>
            {operation}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export const getFirstSymbol = (val: string): string => {
  const index = val.indexOf('/');
  if (index < 0) {
    return val;
  }
  return val.substring(0, index);
};

const getNumericVal = (val: string): string => {
  let result = '';
  for (let i = 0; i < val.length; i++) {
    const c = val.charAt(i);
    if ((c >= '0' && c <= '9') || c === '.') {
      result += c;
    }
  }
  if (result) {
    return result; //parseFloat(result);
  }
  return '0';
};

const styles = StyleSheet.create({
  amountTextInput: {
    marginTop: 10,
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 13,
  },
  amountTextView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  actionRow: {
    //marginTop: 20,
    borderTopColor: Colors.grey400,
    borderTopWidth: 1,
    flexDirection: 'row',
    height: 60,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actionText: {
    marginLeft: 10,
    fontSize: 16,
  },
  actionValue: {},
  infoIcon: {
    borderColor: Colors.grey500,
    height: 40,
    borderWidth: 1,
    borderRadius: 20,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
  },
  headerInfo: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: -10,
    marginRight: 30,
  },
  viewTitleValue: {
    //marginTop: 10,
    //flexDirection: 'row',
    //justifyContent: 'space-between',
    //alignItems: 'center',
  },
  actionButton: {
    backgroundColor: Colors.blue600,
    height: 60,
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
});
