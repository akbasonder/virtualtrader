/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
  FlatList,
  Pressable,
  ScrollView,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Text, Colors} from 'react-native-paper';
import {gql} from '@apollo/client';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {TimeLineGraph, PERIOD} from '../common/Graph';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const logger = new Logger('Statistics');

const SCREEN_WIDTH = Dimensions.get('window').width;

export const StatisticsPage = () => {
  const [selectedPeriod, setSelectedPeriod] = useState(PERIOD.DAY);
  //const route = useRoute();
  //const {period} = route.params;
  const navigation = useNavigation();

  const testData = [
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,

    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
    Math.random() * 100,
  ];
  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <View
        style={{
          flexDirection: 'row',
          //backgroundColor: Colors.blue100,
          alignItems: 'center',
          justifyContent: 'space-between',
          borderBottomColor: Colors.grey400,
          borderBottomWidth: 1,
        }}>
        <Pressable
          style={{
            height: 50,
            width: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => navigation.goBack()}>
          <MaterialIcons name="arrow-back-ios" size={30} />
        </Pressable>

        <Text> Ripple (XRP) </Text>
        <Pressable style={styles.starIcon} onPress={() => Alert.alert('star')}>
          <MaterialIcons
            size={30}
            name={'star'}
            color={Colors.yellow800}
            //style={{marginLeft: 0, marginTop: -5}}
          />
        </Pressable>
      </View>
      <View>
        <View
          style={{
            flexDirection: 'row',
            height: 50,
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottomColor: Colors.grey400,
            borderBottomWidth: 1,
          }}>
          <Text
            style={{
              marginLeft: 25,
              fontSize: 20,
              fontWeight: '600',
              color: Colors.grey900,
            }}>
            47.317,71 USD
          </Text>
          <View style={{marginRight: 25}}>
            <Text>-0.08%</Text>
            <Text>-1300</Text>
          </View>
        </View>
        <TimeLineGraph
          period={selectedPeriod}
          data={testData}
          height={SCREEN_WIDTH * 0.6}
          backgroundColor={Colors.white}
        />
        <View
          style={{
            //borderColor: Colors.grey400,
            //borderWidth: 1,
            marginTop: -10,
            flexDirection: 'row',
            height: 30,
            justifyContent: 'space-between',
            marginLeft: 20,
            marginRight: 20,
          }}>
          <TouchableOpacity
            style={
              selectedPeriod === PERIOD.DAY
                ? styles.selectedTime
                : styles.notSelectedTime
            }
            onPress={() => {
              setSelectedPeriod(PERIOD.DAY);
            }}>
            <Text>Day</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              selectedPeriod === PERIOD.WEEK
                ? styles.selectedTime
                : styles.notSelectedTime
            }
            onPress={() => {
              setSelectedPeriod(PERIOD.WEEK);
            }}>
            <Text>Week</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              selectedPeriod === PERIOD.MONTH
                ? styles.selectedTime
                : styles.notSelectedTime
            }
            onPress={() => {
              setSelectedPeriod(PERIOD.MONTH);
            }}>
            <Text>Month</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={
              selectedPeriod === PERIOD.YEAR
                ? styles.selectedTime
                : styles.notSelectedTime
            }
            onPress={() => {
              setSelectedPeriod(PERIOD.YEAR);
            }}>
            <Text>Year</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            //justifyContent: 'space-around',
            justifyContent: 'center',
            height: 50,
            marginTop: 10,
            borderTopWidth: 1,
            borderTopColor: Colors.grey400,
          }}>
          {/*<TouchableOpacity
            onPress={() => navigation.navigate('TradeNav', {isSell: true})}
            style={styles.actionButton}>
            <Text style={styles.actionText}>Sell</Text>
            <Text style={styles.actionText}>44.300</Text>
          </TouchableOpacity>*/}
          <TouchableOpacity
            style={styles.actionButton}
            onPress={() => navigation.navigate('TradeNav', {isSell: false})}>
            <Text style={styles.actionText}>Buy</Text>
            <Text style={styles.actionText}>44.500</Text>
          </TouchableOpacity>
        </View>
        {/*<View style={{height: 60}} />*/}
      </View>
    </View>
  );
};

type TranscationListType = {
  item: TranscationType;
  index: number;
};

type TranscationType = {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  currentUnitPrice: number;
  purchasedUnitPrice: number;
  //changeTotal: number;
  //changePercantage: number;
};

const styles = StyleSheet.create({
  headerInfo: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 30,
  },
  starIcon: {
    borderColor: Colors.grey500,
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectedTime: {
    borderRadius: 15,
    width: 60,
    backgroundColor: Colors.grey400,
    justifyContent: 'center',
    alignItems: 'center',
  },
  notSelectedTime: {
    borderRadius: 15,
    width: 60,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: Colors.grey800,
  },
  actionButton: {
    width: 150,
    marginTop: 20,
    backgroundColor: Colors.blue600,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    borderRadius: 25,
  },
  actionText: {
    color: Colors.white,
    fontWeight: '500',
    fontSize: 16,
  },
});
