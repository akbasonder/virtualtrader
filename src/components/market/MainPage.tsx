/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Alert,
  FlatList,
  Pressable,
} from 'react-native';
import {Logger} from '../../utils/Logger';
import {useNavigation, useRoute} from '@react-navigation/native';
import {
  Text,
  Colors,
  Searchbar,
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
} from 'react-native-paper';
import {gql, useQuery} from '@apollo/client';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
//import IonIcons from 'react-native-vector-icons/Ionicons';
import {createStackNavigator} from '@react-navigation/stack';
//import {createState, useState} from '@hookstate/core';
import {atom, useRecoilState, useRecoilValue} from 'recoil';
import {MarketItem, MarketPage} from './Market';
import {TradePage} from './Trade';
import {StatisticsPage} from './Statistics';
import {InfoPage} from './Info';
import {ShowError, ShowLoading} from '../portfolio/Portfolio';

export const searchTextState = atom({
  key: 'searchTextState', // unique ID (with respect to other atoms/selectors)
  default: '', // default value (aka initial value)
});

const logger = new Logger('MainPage');

const MarketTopStack = createMaterialTopTabNavigator();
const MarketMainStack = createStackNavigator();

export const GET_ASSETS = gql`
  query assets($category: String, $capitalName: String, $priority: Int) {
    assets(
      take: 10
      where: {
        category: {equals: $category}
        capitalName: {contains: $capitalName}
        priority: {lte: $priority}
      }
      orderBy: {rank: asc}
    ) {
      id
      open
      symbol
      ask
      bid
      name
      price
      capitalName
    }
  }
`;

export const MarketTopTopNav = () => {
  const [searchText, setSearchText] = useRecoilState(searchTextState);
  const onChangeSearch = (query: string) => setSearchText(query); //setSearchQuery(query);
  return (
    <View style={{flex: 1, backgroundColor: Colors.white}}>
      <SafeAreaView style={{flex: 1}}>
        <Searchbar
          style={{borderColor: Colors.grey400, borderWidth: 1}}
          placeholder="Search"
          onChangeText={onChangeSearch}
          value={searchText}
        />
        <View style={{flex: searchText ? 0 : 1}}>
          <MarketMainStack.Navigator headerMode="none">
            <MarketMainStack.Screen
              name="MarketTopStackNav"
              component={MarketTopStackNav}
            />
            <MarketMainStack.Screen
              name="MarketPageNav"
              component={MarketPage}
            />
            <MarketMainStack.Screen name="TradeNav" component={TradePage} />
            <MarketMainStack.Screen
              name="StatisticsNav"
              component={StatisticsPage}
            />
            <MarketMainStack.Screen name="InfoNav" component={InfoPage} />
          </MarketMainStack.Navigator>
        </View>
        {searchText ? <SearchResultPage /> : <View />}
      </SafeAreaView>
    </View>
  );
};

const SearchResultPage = () => {
  const searchText = useRecoilValue(searchTextState);
  const {loading, data, error} = useQuery(GET_ASSETS, {
    variables: {capitalName: searchText.toUpperCase()},
  });
  return (
    <View style={{flex: 1, backgroundColor: Colors.white, marginTop: 20}}>
      {loading ? (
        <ShowLoading />
      ) : error ? (
        <ShowError error={error} />
      ) : (
        <FlatList
          data={data.assets}
          //renderItem={(item)=><MarketItem id={item.item.id}/>}
          //renderItem={MarketItem}
          renderItem={({item}) => <MarketItem item={item} />}
          keyExtractor={(item) => item.id.toString()}
          contentInset={{right: 0, top: 0, left: 0, bottom: 50}}
        />
      )}
    </View>
  );
};

const PopularsPage = () => {
  const {loading, data, error} = useQuery(GET_ASSETS, {
    variables: {priority: 1},
  });
  if (data) {
    logger.debug('popularspage data,', data);
  }
  return (
    <View style={{flex: 1, marginTop: 5}}>
      {loading ? (
        <ShowLoading />
      ) : error ? (
        <ShowError error={error} />
      ) : (
        <FlatList
          data={data.assets}
          //renderItem={(item)=><MarketItem id={item.item.id}/>}
          //renderItem={MarketItem}
          renderItem={({item}) => <MarketItem item={item} />}
          keyExtractor={(item) => item.id.toString()}
          contentInset={{right: 0, top: 0, left: 0, bottom: 50}}
        />
      )}
    </View>
  );
};

const MarketTopStackNav = () => {
  return (
    <MarketTopStack.Navigator>
      <MarketTopStack.Screen
        name="allMarketsNav"
        options={{title: 'All'}}
        component={AllMarkets}
      />
      <MarketTopStack.Screen
        name="favoriteMarketsNav"
        options={{title: 'Populars'}}
        component={PopularsPage}
      />
      <MarketTopStack.Screen
        name="popularMarketsNav"
        options={{title: 'Favorites'}}
        component={Empty}
      />
    </MarketTopStack.Navigator>
  );
};

const Empty = () => {
  return <View style={{flex: 1}}></View>;
};

const AllMarkets = () => {
  return (
    <View style={{flex: 1}}>
      <View
        style={{
          height: 10,
          borderBottomColor: Colors.grey400,
          borderBottomWidth: 1,
        }}
      />
      <Market name="Forex" />
      <Market name="Commodity" />
      <Market name="Crypto" />
      <Market name="Indices" />
      <Market name="Stock" />
    </View>
  );
};

type MarketType = {
  name: string;
};
const Market = ({name}: MarketType) => {
  const navigation = useNavigation();

  // navigation.navigate(navigateParam)
  return (
    <Pressable
      style={{
        height: 70,
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomColor: Colors.grey400,
        borderBottomWidth: 1,
      }}
      onPress={() => navigation.navigate('MarketPageNav', {name})}>
      <Text style={{marginLeft: '10%', width: '80%', fontSize: 18}}>
        {name}
      </Text>

      <MaterialIcons name="keyboard-arrow-right" size={30} />
    </Pressable>
  );
};
